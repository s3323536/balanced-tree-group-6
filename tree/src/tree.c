#include "tree.h"

/**
 * Please correct the contents of this file to make sure all functions here do what they are supposed to do if you find
 * that they do not work as expected.
 */

// Tree function: you are allowed to change the contents, but not the method signature
Tree* tree_create(){
    Tree *tree = malloc(sizeof(Tree));
    tree->root = NULL;

    return tree;
}

// Helper function: you are allowed to change this to your preferences
void tree_node_delete(Node* node) {
    if (node == NULL) return;

    //printf("Deleting node %p: %p | %p\n",node,node->left,node->right);

    if (node->left != NULL){
        //printf("Going left\n");
        tree_node_delete(node->left);
        node->left = NULL;
    }
    if (node->right != NULL){
        //printf("Going right\n");
        tree_node_delete(node->right);

        node->right = NULL;
    }
    //printf("Deleting node %p\n",node);
    if(node->name != NULL){
        free(node->name);
        node->name = NULL;
    }

    free(node);
    node = NULL;
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_delete(Tree* tree) {
    if (tree == NULL){
        return;
    }
    //printf("Deleting tree: %p\n",tree->root);
    tree_node_delete(tree->root);

    //printf("Tree root in tree_delete: %p\n",tree->root);
    tree->root = NULL;
    free(tree);
}

// Helper function: you are allowed to change this to your preferences
void node_insert(Node* node, int age, char* name) {
    if(node == NULL || name == NULL){
        printf("ERROR Invalid arguments for node_insert");
        return;
    }
    if (age <= node->age){
        if (node->left == NULL){
            Node* newLeft = malloc(sizeof(Node));
            if (newLeft == NULL){
                return;
            }
            newLeft->age = age;
            newLeft->name = name;
            newLeft->left = NULL;
            newLeft->right = NULL;
            node->left = newLeft;
        } else {
            node_insert(node->left, age, name);
        }
    } else {
        if (node->right == NULL){
            Node* newRight = malloc(sizeof(Node));
            if (newRight == NULL){
                return;
            }
            newRight->age = age;
            newRight->name = name;
            newRight->left = NULL;
            newRight->right = NULL;
            node->right = newRight;
        } else {
            node_insert(node->right, age, name);
        }
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_insert(Tree* tree, int age, char* name) {
    if (tree == NULL){
        printf("ERROR Tree given to tree_insert is NULL! Perhaps run tree_create before?\n");
        return;
    }
    if(name == NULL){
        printf("ERROR Name given to tree_insert is null\n");
        return;
    }
    if (tree->root == NULL) {
        Node *node = malloc(sizeof(Node));
        if (node == NULL){
            printf("ERROR Failed to allocate space for new node\n");
            return;
        }
        node->name = name;
        node->age = age;
        node->left = NULL;
        node->right = NULL;
        tree->root = node;
        //printf("Created root with name %s and age %d\n",name,age);
    } else {
        node_insert(tree->root, age, name);
    }
}

//Same as node_find but tries to find the parent of the node instead of the node itself
Node* node_find_parent(Node* node, int age, char* name) {
    if (node == NULL || name == NULL){
        return NULL;
    }
    if (node->left != NULL){
        if (node->left->age == age && !strcmp(node->left->name, name)) {
            return node;
        }else if (age <= node->age) {
            return node_find_parent(node->left, age, name);
        }
    }
    if (node->right != NULL){
        if (node->right->age == age && !strcmp(node->right->name, name)) {
            return node;
        }else {
            return node_find_parent(node->right, age, name);
        }
    }

    return NULL;

}

//Same as tree_find but tries to find the parent of the node instead of the node itself
Node* tree_find_parent(Tree* tree, int age, char* name) {
    if (tree == NULL || name == NULL){
        return NULL;
    }
    return node_find_parent(tree->root, age, name);
}


//Helper function
Node* node_find_maximum_parent(Node* node){
    if (node->right == NULL){
        return NULL;
    }
    if (node->right->right == NULL){
        return node;
    }
    node_find_maximum_parent(node->right);
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_erase(Tree* tree, int age, char* name) {

    //printf("Erasing node\n");

    //We are supposed to remove root node
    Node* node_to_erase_parent = NULL;
    Node* node_to_erase = NULL;
    int node_is_left = 0;
    //Check if node to erase is the root
    if(tree->root->age == age && !strcmp(tree->root->name,name)){
        //printf("Node to erase is root\n");
        node_to_erase = tree->root;
    //If not we must find the node!
    }else{
        //printf("Node to erase is not root, finding parent\n");
        //First we find its parent
        node_to_erase_parent = tree_find_parent(tree, age, name);
        //Node wasnt found
        if (node_to_erase_parent == NULL){
            //printf("Node not found\n");
            return;
        }
        //printf("Parent found\n");
        //Then we find the node and keep track if it was at the left or right of its parent
        if(node_to_erase_parent->left!= NULL){
            //printf("Left is not null %d %d\n",node_to_erase_parent->left->age == age ,!strcmp(node_to_erase_parent->left->name,name));
            if (node_to_erase_parent->left->age == age && !strcmp(node_to_erase_parent->left->name,name)){
                node_to_erase = node_to_erase_parent->left;
                node_is_left = 1;
                //printf("Found node to erase as child on left\n");
            }
        }
        if(node_to_erase_parent->right!= NULL){
            //printf("Right is not null\n");
            if (node_to_erase_parent->right->age == age && !strcmp(node_to_erase_parent->right->name,name)){
                node_to_erase = node_to_erase_parent->right;
                //printf("Found node to erase as child on right\n");
            }
        }else{
            printf("ERROR found parent node but child node is not present!\n");
        }
        //printf("Supposed to erase %d-%s, parent is %d-%s\n",node_to_erase->age,node_to_erase->name,node_to_erase_parent->age,node_to_erase_parent->name);
    }

    //printf("Switching nodes\n");

    //Case in which the node to remove has nodes to the left and right
    if(node_to_erase->left != NULL && node_to_erase->right != NULL){

        //Get the maximum node from the left subtree to susbstitute by the node we are erasing
        //printf("finding maximum parent\n");
        Node* max_node_left_tree_parent = NULL;
        Node* max_node_left_tree = NULL;
        if(node_to_erase->left->right == NULL){
            max_node_left_tree = node_to_erase->left;
        }else{
            max_node_left_tree_parent = node_find_maximum_parent(node_to_erase->left);
            max_node_left_tree = max_node_left_tree_parent->right;
            //printf("Checking maximum node leaves\n");
            if (max_node_left_tree->left != NULL){
                max_node_left_tree_parent->right = max_node_left_tree->left;
            }else{
                max_node_left_tree_parent->right = NULL;
            }
        }

        //printf("Replacing node to erase with left maximum\n");
        //Switch node to erase by the maximum node of left subtree
        if(node_to_erase_parent != NULL){
            if (node_is_left){
                node_to_erase_parent->left = max_node_left_tree;
            }else{
                node_to_erase_parent->right = max_node_left_tree;
            }
        //Case in wich node to erase is the root
        }else{
            tree->root = max_node_left_tree;
        }
        //printf("Setting children of new root\n");
        if(node_to_erase->left->right != NULL){
            max_node_left_tree->left = node_to_erase->left;
        }
        max_node_left_tree->right = node_to_erase->right;
    //If node to erase only has nodes to the left, replace itself with root of left subtree
    }else if(node_to_erase->left != NULL){
        //printf("Node to erase only has nodes to the left\n");
        //If it is not the root
        if(node_to_erase_parent != NULL){
            if(node_is_left){
                node_to_erase_parent->left = node_to_erase->left;
            }else{
                node_to_erase_parent->right = node_to_erase->left;
            }
        }else{
            tree->root = tree->root->left; 
        }
    //If node to erase only has nodes to the right, replace itself with root of right subtree
    }else if(node_to_erase->right != NULL){
        //printf("Node to erase only has nodes to the right\n");
        //If it is not the root
        if(node_to_erase_parent != NULL){
            if(node_is_left){
                node_to_erase_parent->left = node_to_erase->right;
            }else{
                node_to_erase_parent->right = node_to_erase->right;
            }
        }else{
            tree->root = tree->root->right; 
        }
    }else{
        //printf("Node to erase has no child nodes\n");
        if(node_is_left && node_to_erase_parent != NULL){
            node_to_erase_parent->left = NULL;
        }else if (node_to_erase_parent != NULL){
            node_to_erase_parent->right = NULL;
        }
    }

    //If we are erasing the root
    if(node_to_erase == tree->root && node_to_erase->left == NULL && node_to_erase->right == NULL){
        tree->root = NULL;
    }

    //printf("Removing node(root=%p)\n",tree->root);
    
    free(node_to_erase->name);
    node_to_erase->name = NULL;
    free(node_to_erase);
    node_to_erase = NULL;
}



// Helper function: you are allowed to change this to your preferences
void tree_print_node(Node* node){

    if (node == NULL) {
        printf("null");
        return;
    }

    //printf("{Printing %s}",node->name);

    printf("[");
    printf("{\"%d\":\"%s\"},", node->age, node->name);
    tree_print_node(node->left);
    printf(",");
    tree_print_node(node->right);
    printf("]");
}

// Tree function: you are allowed to change the contents, but not the method signature
void tree_print(Tree* tree, int printNewline){
    if (tree == NULL) {
        printf("null");
        return;
    }else{
        tree_print_node(tree->root);
    }

    if (printNewline){
        printf("\n");
    }
}

// Helper function: you are allowed to change this to your preferences
Node* node_find(Node* node, int age, char* name) {
    //printf("Checkin node %p\n",node);
    //Check the parameters
    if (node == NULL || name == NULL){
        return NULL;
    }
    //Check if we found the node
    if (node->age == age && !strcmp(node->name, name)) {
        return node;
    }
    //Check wether to go left or right
    if (age <= node->age) {
        //If we reach a dead end we return NULL, the node is not in the tree
        if (node->left == NULL){
            return NULL;
        }
        //Search in the left subtree
        return node_find(node->left, age, name);
    } else {
        //If we reach a dead end we return NULL, the node is not in the tree
        if (node->right == NULL){
            return NULL;
        }
        //Search on the right subtree
        return node_find(node->right, age, name);
    }
}

// Tree function: you are allowed to change the contents, but not the method signature
Node* tree_find(Tree* tree, int age, char* name) {
    if (tree == NULL || name == NULL || tree->root == NULL){
        return NULL;
    }
    //printf("Finding node in tree %p with name %p\n",tree,name);
    return node_find(tree->root, age, name);
}
