/**
 * This file exists to help you check if your tree is working correctly. You can run it with the following command line:
 *
 * You are allowed to add or remove parts of the test code if you wish. The contents of this file will not be used to
 * grade your assignment.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tree.h"



int test_node(Node* node, int parent_age,char* side, int isRoot){
    if (node == NULL){
        return 1;
    }
    if(!test_node(node->left,node->age,"left",0)){
        return 0;
    }
    if(!test_node(node->right,node->age,"right",0)){
        return 0;
    }
    if (!isRoot){
        if (side == "left"){
            if (node->age > parent_age){
                return 0;
            }
        }else if(side == "right"){
            if(node->age < parent_age){
                return 0;
            }
        }
    }
    return 1;

}


// You are allowed to change anything about this function to your preferences
int main(){
    Tree* tree = tree_create();

    printf("Checking basic functionality doesn't crash...\n");
    char* n1 = malloc(sizeof(char) * 20);
    strcpy(n1,"Peter");
    char* n2 = malloc(sizeof(char) * 20);
    strcpy(n2,"Joanna");
    char* n3 = malloc(sizeof(char) * 20);
    strcpy(n3,"Margareth");
    tree_insert(tree, 42, n1);
    tree_insert(tree, 21, n2);
    tree_insert(tree, 83, n3);

    printf("Checking tree print method...\n");
    tree_print(tree, 1);

    printf("Testing find method...\n");
    if (!tree_find(tree, 83, "Margareth")){
        fprintf(stderr, "Could not find an item that was recently inserted\n");
        return 1;
    }

    printf("Testing erase method...\n");
    tree_erase(tree, 83, "Margareth");
    if(tree_find(tree, 83, "Margareth")){
        fprintf(stderr, "Found an item that was supposed to be removed (leaf node)\n");
        return 3;
    }
    printf("Margareth erased successfully\n");
    char* n4 = malloc(sizeof(char) * 20);
    strcpy(n4,"Margareth");
    tree_insert(tree, 83, n4);
    tree_erase(tree, 42, "Peter");
    if(tree_find(tree, 42, "Peter")){
        fprintf(stderr, "Found an item that was supposed to be removed (node with children)\n");
        return 4;
    }

    printf("Peter erased successfully\n");

    
    tree_delete(tree);
    tree = NULL;

    printf("Cleaned tree. Starting next phase of testing %p\n\n",tree);

    printf("Stress testing your tree (to help you check for possible memory errors)\n");

    tree = tree_create();

    //printf("tree root: %p\n",tree->root);

    // This makes the results reproducable
    printf("Testing the tree\n");
    srand(0xC0FFEE);
    //srand(time(NULL));
    for (int i = 0; i < 1000000; i++){
        int age = (int) (random() % 10000000);
        char* name = malloc(sizeof(char) * 20);
        snprintf(name,19, "Name%d", age);
        tree_insert(tree, age, name);
        age = (int) (random() % 100);
        tree_erase(tree, age, name);
    }


    if(test_node(tree->root,0,"",1)){
        printf("SUCCESS Tree is sorted!\n");
    }else{
        printf("ERROR Tree is not sorted!\n");
        return 11;
    }

    // Feel free to add your own tests here or somewhere else in this file


    tree_delete(tree);

    printf("The test succeeded\n");
    return 0;
}