# CMake generated Testfile for 
# Source directory: /home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests
# Build directory: /home/joaobcosta2001/Desktop/balanced-tree-group-6/build/main/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_main "/usr/bin/bash" "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/test_main.sh" "/home/joaobcosta2001/Desktop/balanced-tree-group-6/build/main/src/main")
set_tests_properties(test_main PROPERTIES  WORKING_DIRECTORY "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests" _BACKTRACE_TRIPLES "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/CMakeLists.txt;2;add_test;/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/CMakeLists.txt;0;")
add_test(test_sorted "/usr/bin/bash" "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/test_sorted.sh" "/home/joaobcosta2001/Desktop/balanced-tree-group-6/build/main/src/main")
set_tests_properties(test_sorted PROPERTIES  WORKING_DIRECTORY "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests" _BACKTRACE_TRIPLES "/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/CMakeLists.txt;3;add_test;/home/joaobcosta2001/Desktop/balanced-tree-group-6/main/tests/CMakeLists.txt;0;")
