# balanced-tree-group-6

First assignment for the course Software Security, in which a sorted tree will be implemented, taking into special consideration all possible security challenges that may arise.

## Implementation

The first step was debugging tree.c and main.c. This meant going through every function and carefully analyzing the algorithms, adding checks for the function parameters and correcting all sorts of bugs.

Some notorious bug fixes were:

- The logic in tree_erase was completly wrong, since when a node was deleted it was just erased, and it wasn't dereferenced from other nodes and the tree wasn't sorted again
- String comparison was made using '==' while it must use strcmp
- Uniformize string declaration to allways use mallocs and not double quotes sometimes.
- Initialize all memory after mallocs

## Testing

During the debugging process, multiple tests where made.

- CMake tests where constantly performed, specially the last test (test_tree.c), which was altered in the end to stress test the  tree with 100 000 nodes, and check if the tree would remain sorted after a series of inserts and erases.
- Valgrind was used to check for potential memory leaks
- Manual testing, to check the behaviour of, for example, the delete command

## Result

We believe the tree is quite 'bulletproof' and should be robust to all inputs and cases, altough it is not always balanced.