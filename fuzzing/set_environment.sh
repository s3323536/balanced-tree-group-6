#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root or using sudo."
  exit 1
fi

cd /

echo "core" > /proc/sys/kernel/core_pattern
cd /sys/devices/system/cpu
echo performance | tee cpu*/cpufreq/scaling_governor


# Check the command execution status
if [ $? -eq 0 ]; then
  echo "Success"
else
  echo "Script failed"
fi
