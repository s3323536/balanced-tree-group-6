#This script takes a file with n lines and creates n files, each one containing a line
#Usage: python3 file_splitter.py [file]

import sys
import os

if len(sys.argv) != 2:
    print("Usage: python3 file_splitter.py [file]")
    sys.exit(1)

file_path = sys.argv[1]

file_count = 0

try:
    with open(file_path, 'r') as master_file:
        lines = master_file.readlines()
        for i,line in enumerate(lines):
            file_name,file_extension = os.path.splitext(file_path)
            try:
                with open(file_name + str(i) + file_extension,'w') as new_file:
                    new_file.write(line)
                    file_count += 1
            except Exception as e:
                print(f"An error occurred: {e}")




except FileNotFoundError:
    print(f"The file {file_path} does not exist.")
except Exception as e:
    print(f"An error occurred: {e}")

print(f"Done! {file_count} files generated.")