#!/bin/bash

# Check if the script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run this script as root or using sudo."
  exit 1
fi

# Specify the paths of files in different categories

# Copy files to the current directory
cp "../main/inc/main.h" "."
cp "../main/src/main.c" "."
cp "../tree/inc/tree.h" "."
cp "../tree/src/tree.c" "."


# Check the file copy status
if [ $? -eq 0 ]; then
  echo "Files copied successfully."

  # Run terminal commands
  echo "Compiling"
  # Add your terminal commands here
  clang -o tree -c tree.c
  afl-clang -o main-fuzz main.c tree.o

  # Check the command execution status
  if [ $? -eq 0 ]; then
    echo "Success"
  else
    echo "Error compiling."
  fi

else
  echo "Error copying files."
fi
